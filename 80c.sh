#!/bin/sh
#
# Grabs the HTM, CSS and js from a pad
# Removes harcoded style
# Then saves the HTML to the file index.html and inserts links to generated
# CSS and JS

# Because I needed to test that elsewhere
if [ "$(uname)" = "FreeBSD" ]
then
  SED="gsed"
else
  SED="sed"
fi

# Where is the pad?
URL=https://annuel.framapad.org/p/eightycolumn.net

# Get HTTP response header from server
RESPONSE=$(curl -I ${URL} | grep 'HTTP/2 200')

if [ -n "${RESPONSE}" ];
then 
  curl ${URL}/export/html | tr '\n' '\r' |\
    ${SED} -e 's/<style>.*<\/style>//' | tr '\r' '\n' |\
    ${SED} -e '/<\/head>/ i\<link rel=\"stylesheet\" type=\"text\/css\" href=\"80c.css\">' \
    > ./build/index.html

  # Grabs the CSS from the pad http://relearn.be/ether/p/80c.css
  curl ${URL}.css/export/txt > ./build/80c.css

else
  echo "Server is down or page unreachable :("
fi
