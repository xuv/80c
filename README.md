80c.sh
======

Just a shell script to update the website of http://eightycolumn.net/
from pads available on:
 * http://relearn.be/ether/p/80c for content
 * http://relearn.be/ether/p/80c.css for CSS
 * http://relearn.be/ether/p/80c.js for Javascript

Copy everything that is in the build/ folder to the root of the website.
Don't forget the karla/ font folder.

This script is unlicensed. http://unlicense.org/  
Karla font is Copyright (c) 2011, Jonathan Pinhorn and released under SIL OPEN FONT LICENSE Version 1.1